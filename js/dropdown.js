(function () {

    angular.module('allegroTask').component('dropdown', {
        templateUrl: 'html/dropdown.html',
        controller: DropdownController,
        bindings: {
            active: '<',
            data: '<',
            label: '<',
            model: '='
        }
    });

    function DropdownController($attrs) {
        var ctrl = this;
        ctrl.showOptions = false;
        ctrl.activated = false;

        this.$onInit = function () {
            ctrl.prefillDropdown();
            ctrl.prefillLabel();
        };

        ctrl.openOptions = function () {
            if (ctrl.active) { ctrl.showOptions = true; }
            ctrl.activated = true;
        };

        ctrl.closeOptions = function () {
            if (ctrl.active) { ctrl.showOptions = false; }
        };

        ctrl.prefillDropdown = function () {
            if (!ctrl.model) { return false; }

            var flag = false;

            angular.forEach(ctrl.data, function (value) {
                if (ctrl.model == value) flag = true;
            });

            if (!flag) ctrl.model = null;
        };
        
        ctrl.checkError = function () {
            return !(ctrl.model || !ctrl.activated || ctrl.showOptions);
        };

        ctrl.validate = function () {
            return ctrl.model ? true : false
        };

        ctrl.setModel = function(value) {
            ctrl.model = value;
            ctrl.closeOptions()
        };

        ctrl.prefillLabel = function () {
            ctrl.label = ctrl.label ? ctrl.label : 'Wybierz';
        }

    }

})();